package com.matm.matmsdk.aepsmodule;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryResponse;
import com.matm.matmsdk.aepsmodule.bankspinner.BankNameModel;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.DeviceInfo;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.MorphoPidData;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.Opts;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.PidData;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.PidOptions;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.uid.AuthReq;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.uid.AuthRes;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.uid.Meta;
import com.matm.matmsdk.aepsmodule.fingerprintmodel.uid.Uses;
import com.matm.matmsdk.aepsmodule.signer.XMLSigner;
import com.matm.matmsdk.aepsmodule.transactionstatus.TransactionStatusAeps2Activity;
import com.matm.matmsdk.aepsmodule.transactionstatus.TransactionStatusModel;
import com.matm.matmsdk.aepsmodule.utils.AepsSdkConstants;
import com.matm.matmsdk.aepsmodule.utils.Session;
import com.matm.matmsdk.aepsmodule.utils.Util;

import org.json.JSONException;
import org.json.JSONObject;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import isumatm.androidsdk.equitas.R;

public class BioAuthActivity extends AppCompatActivity {
    private UsbDevice usbDevice;
    boolean usbconnted = false;
    String deviceSerialNumber = "0", balanceaadharNo = "";
    String morphodeviceid = "SAGEM SA";
    String mantradeviceid = "MANTRA";
    String morphoe2device = "Morpho";
    UsbManager musbManager;
    ImageView two_fact_fingerprint;
    ProgressBar two_fact_depositBar;
    Button two_fact_submitButton;
    private MorphoPidData morphoPidData;
    private Serializer serializer;
    private ArrayList<String> positions;
    ProgressDialog loadingView;
    Session session;
    private PidData pidData;
    TextView userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio_auth);
        positions = new ArrayList<>();
        serializer = new Persister();
        session = new Session(BioAuthActivity.this);
        session.setUserToken(AepsSdkConstants.tokenFromCoreApp);
        session.setUsername(AepsSdkConstants.userNameFromCoreApp);

        musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        updateDeviceList();
        two_fact_fingerprint = findViewById(R.id.two_fact_fingerprint);
        two_fact_depositBar = findViewById(R.id.progressBar);
        two_fact_submitButton = findViewById(R.id.two_fact_submitButton);
        RadioButton bl_aadhar_no_rd = findViewById(R.id.bl_aadhar_no_rd);
        RadioButton bl_aadhar_uid_rd = findViewById(R.id.bl_aadhar_uid_rd);
        EditText balanceAadharNumber = findViewById(R.id.balanceAadharNumber);
        EditText balanceAadharVID = findViewById(R.id.balanceAadharVID);

        userName = findViewById(R.id.userName);
        userName.setText("Hi, "+AepsSdkConstants.applicationUserName);

        balanceAadharNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 1) {
                    balanceAadharNumber.setError(getResources().getString(R.string.aadhaarnumber));

                }

                if (s.length() > 0) {
                    balanceAadharNumber.setError(null);
                    String aadharNo = balanceAadharNumber.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharNumber(aadharNo) == false) {
                        balanceAadharNumber.setError(getResources().getString(R.string.valid_aadhar_error));
                    }
                }
            }
        });

        balanceAadharVID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 1) {
                    balanceAadharVID.setError(getResources().getString(R.string.aadhaarnumber_vid));

                }

                if (s.length() > 0) {
                    balanceAadharVID.setError(null);
                    String aadharNo = balanceAadharVID.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharVID(aadharNo) == false) {
                        balanceAadharVID.setError(getResources().getString(R.string.valid_aadhar__uid_error));
                    }
                }
            }
        });

        two_fact_fingerprint.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                //showLoader ();


                if (bl_aadhar_no_rd.isChecked()) {
                    balanceaadharNo = balanceAadharNumber.getText().toString();
                    if (balanceaadharNo.contains("-")) {
                        balanceaadharNo = balanceaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharNumber(balanceaadharNo) == false) {
                        balanceAadharNumber.setError(getResources().getString(R.string.valid_aadhar_error));
                        return;
                    }
                }

                if (bl_aadhar_uid_rd.isChecked()) {
                    balanceaadharNo = balanceAadharVID.getText().toString();
                    if (balanceaadharNo.contains("-")) {
                        balanceaadharNo = balanceaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharVID(balanceaadharNo) == false) {
                        balanceAadharVID.setError(getResources().getString(R.string.valid_aadhar__uid_error));
                        return;
                    }
                }


               /* musbManager = (UsbManager) getSystemService( Context.USB_SERVICE);
                updateDeviceList ();
                */
                if (usbDevice != null) {
                    if (usbDevice.getManufacturerName().trim().equalsIgnoreCase(mantradeviceid)) {
                        //Toast.makeText ( DashboardActivity.this, "devicemantra"+usbDevice.getManufacturerName (), Toast.LENGTH_SHORT ).show ();
                        biocapture();
                    } else if (usbDevice.getManufacturerName().trim().equalsIgnoreCase(morphodeviceid) || usbDevice.getManufacturerName().trim().equalsIgnoreCase(morphoe2device)) {
                        // Toast.makeText ( DashboardActivity.this, "devicemorpho"+usbDevice.getManufacturerName (), Toast.LENGTH_SHORT ).show ();
                        biomorophoCapture();
                    }
                } else {
                    musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                    updateDeviceList();
//                    deviceConnectMessgae ();
                }
            }
        });
        two_fact_submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pidData != null) {
                    new AuthRequestINDUS(balanceaadharNo, pidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });
    }

    /*
     *
     * usbmanger is checking the connection
     *
     * wether a usb device is connnected to the device or not
     */
    private void updateDeviceList() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        hideLoader();
        if (connectedDevices.isEmpty()) {
            usbconnted = false;
            // Toast.makeText(DashboardActivity.this, "No Devices Currently Connected" + usbconnted, Toast.LENGTH_LONG).show();
            deviceConnectMessgae();
        } else {
            for (UsbDevice device : connectedDevices.values()) {
                usbconnted = true;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (device != null && device.getManufacturerName() != null) {
                        if (device.getManufacturerName().equalsIgnoreCase(mantradeviceid) || device.getManufacturerName().equalsIgnoreCase(morphodeviceid) || device.getManufacturerName().trim().equalsIgnoreCase(morphoe2device)) {
                            usbDevice = device;
                            deviceSerialNumber = usbDevice.getManufacturerName();
                        }
                    }
                }
            }
            devicecheck();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void devicecheck() {
        if (usbDevice == null) {
            deviceConnectMessgae();
        } else {
            if (usbDevice.getManufacturerName().trim().equalsIgnoreCase(mantradeviceid)) {
                installcheck();
            } else if (usbDevice.getManufacturerName().trim().equalsIgnoreCase(morphodeviceid) || usbDevice.getManufacturerName().trim().equalsIgnoreCase(morphoe2device)) {
                morphoinstallcheck();
            }
        }
    }

    private void installcheck() {
        boolean isAppInstalled = appInstalledOrNot("com.mantra.clientmanagement");
        boolean serviceAppInstalled = appInstalledOrNot("com.mantra.rdservice");
        if (isAppInstalled) {
// This intent will help you to launch if the package is already installed
            if (serviceAppInstalled) {
                Intent intent = new Intent("in.gov.uidai.rdservice.fp.INFO");
                intent.setPackage("com.mantra.rdservice");
                startActivityForResult(intent, 1);
            } else {
                rdserviceMessage();

            }
        } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
            mantraMessage();
        }
    }

    private void mantraMessage() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(BioAuthActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(BioAuthActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_client_management_install))
                .setMessage(getResources().getString(R.string.mantra))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.clientmanagement"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    private void rdserviceMessage() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(BioAuthActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(BioAuthActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_install))
                .setMessage(getResources().getString(R.string.mantra_rd_service))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    private void morphoinstallcheck() {
        boolean isAppInstalled = appInstalledOrNot("com.scl.rdservice");
        if (isAppInstalled) {
//This intent will help you to launch if the package is already installed
            Intent intent1 = new Intent();
            intent1.setAction("in.gov.uidai.rdservice.fp.INFO");
            intent1.setPackage("com.scl.rdservice");
//            intent1.addFlags ( Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivityForResult(intent1, 3);
        } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
            morphoMessage();
        }
    }

    private void morphoMessage() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(BioAuthActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(BioAuthActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.morpho))
                .setMessage(getResources().getString(R.string.install_morpho_message))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /*
                         * play store intent
                         */
                        final String appPackageName = "com.scl.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    /*
   app installation check
   */
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    private void deviceConnectMessgae() {
        hideLoader();
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(BioAuthActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(BioAuthActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.device_connect))
                .setMessage(getResources().getString(R.string.setting_device))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                })
                .show();
    }

    private class AuthRequestINDUS extends AsyncTask<Void, Void, String> {

        private String uid, mobile;
        private PidData pidData;
        ProgressDialog dialog;
        private int posFingerFormat = 0;
        Meta meta;
        AuthReq authReq;
        DeviceInfo info;

        private AuthRequestINDUS(String uid, PidData pidData) {
            this.uid = uid;
            this.pidData = pidData;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(BioAuthActivity.this);
            dialog.setMessage("please wait..");
            dialog.setCancelable(false);
            dialog.show();

        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                info = pidData._DeviceInfo;

                Uses uses = new Uses();
                uses.pi = "n";
                uses.pa = "n";
                uses.pfa = "n";
                uses.bio = "y";
                if (posFingerFormat == 1) {
                    uses.bt = "FIR";
                } else {
                    uses.bt = "FMR";
                }
                uses.pin = "n";
                uses.otp = "n";

                meta = new Meta();
                meta.udc = "MANT0";
                meta.rdsId = info.rdsId;
                meta.rdsVer = info.rdsVer;
                meta.dpId = info.dpId;
                meta.dc = info.dc;
                meta.mi = info.mi;
                meta.mc = info.mc;

                authReq = new AuthReq();
                authReq.uid = uid;
                authReq.rc = "Y";
                authReq.tid = "registered";
                authReq.ac = "public";
                authReq.sa = "public";
                authReq.ver = "2.0";
                authReq.txn = generateTXN();
                authReq.lk = "MEaMX8fkRa6PqsqK6wGMrEXcXFl_oXHA-YuknI2uf0gKgZ80HaZgG3A"; //AUA
                authReq.skey = pidData._Skey;
                authReq.Hmac = pidData._Hmac;
                authReq.data = pidData._Data;
                authReq.meta = meta;
                authReq.uses = uses;
                authReq.freshnessFactor = "";

                StringWriter writer = new StringWriter();
                serializer.write(authReq, writer);
                String pass = "public";
                String reqXML = writer.toString();
                String signAuthXML = XMLSigner.generateSignXML(reqXML, getAssets().open("staging_signature_privateKey.p12"), pass);
                URL url = new URL(getAuthURL(uid));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(30000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/xml");
                conn.setUseCaches(false);
                conn.setDefaultUseCaches(false);
                OutputStreamWriter writer2 = new OutputStreamWriter(conn.getOutputStream());
                writer2.write(signAuthXML);
                writer2.flush();
                conn.connect();

                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response;
                while ((response = reader.readLine()) != null) {
                    sb.append(response).append("\n");
                }
                response = sb.toString();

                AuthRes authRes = serializer.read(AuthRes.class, response);
                String res;
                if (authRes.err != null) {
                    if (authRes.err.equals("0")) {
                        res = "Authentication Success" + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    } else {
                        res = "Error Code: " + authRes.err + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    }
                } else {
                    res = "Authentication Success" + "\n"
                            + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                            + "TXN: " + authRes.txn + "\n"
                            + "";
                }
                return res;
            } catch (Exception e) {
                return "Error: " + e.toString();
            }
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (dialog != null) {
                dialog.dismiss();
            }
            if (res != null && authReq != null && meta != null && info != null) {


                String value = authReq.skey.value.toString();

                //  String last = value.charAt((value.length()) -1);
                String last = String.valueOf(value.charAt(value.length() - 1));
                if (last.equalsIgnoreCase("\n")) {
                    value = value.replace("\n", "");
                }
                JSONObject obj = new JSONObject();
                try {
                    obj.put("aadharNo", uid);
                    obj.put("dpId", meta.dpId);
                    obj.put("rdsId", meta.rdsId);
                    obj.put("rdsVer", meta.rdsVer);
                    obj.put("dc", meta.dc);
                    obj.put("mi", meta.mi);
                    obj.put("mcData", meta.mc);
                    obj.put("sKey", value);
                    obj.put("hMac", authReq.Hmac);
                    obj.put("encryptedPID", authReq.data.value);
                    obj.put("ci", authReq.skey.ci);
                    obj.put("operation", "");
                    submitBioAuthh(mobile, uid, obj);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("", uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, balanceMobileNumber.getText().toString().trim(), "", meta.rdsId, meta.rdsVer, value);
            } else {
                Util.showAlert(BioAuthActivity.this, getResources().getString(R.string.alert_error), getResources().getString(R.string.scan_finger_alert_error));
            }
        }
    }

    /*
     * calendar data for the mantra and morpho
     *
     * capture date and time
     */
    private String generateTXN() {
        try {
            Date tempDate = Calendar.getInstance().getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.ENGLISH);
            String dTTXN = formatter.format(tempDate);
            return dTTXN;
        } catch (Exception e) {
            return "";
        }
    }

    /*
     *  url for the sync of the data for the
     */

    private String getAuthURL(String UID) {
        String url = "http://developer.uidai.gov.in/auth/";
        url += "public/" + UID.charAt(0) + "/" + UID.charAt(1) + "/";
        url += "MG41KIrkk5moCkcO8w-2fc01-P7I5S-6X2-X7luVcDgZyOa2LXs3ELI"; //ASA
        return url;
    }

    private void submitBioAuthh(final String mobile, final String aadhar, JSONObject obj1) {
        showLoader();
        AndroidNetworking.get("https://vpn.iserveu.tech/generate/v80")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            submitBioAuth(mobile, aadhar, obj1, encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }


    private void submitBioAuth(final String mobile, final String aadhar, JSONObject obj, final String encodedUrl) {
        // showLoader();
        AndroidNetworking.post(encodedUrl)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .addHeaders("Authorization", session.getUserToken())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoader();

                        try {
                            // profile_id,is_declaration
                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString("status");
                            if (status.equalsIgnoreCase("0")) {
                                //initView();
                                AepsSdkConstants.bioauth = false;
                                Toast.makeText(BioAuthActivity.this, "SUCCESS", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(BioAuthActivity.this, AEPS2HomeActivity.class));
                                finish();
                            } else {
                                Toast.makeText(BioAuthActivity.this, "FAILURE", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(BioAuthActivity.this, "FAILURE", Toast.LENGTH_SHORT).show();

                    }
                });
    }


    /*
     * Biomectirc device's capture data
     */
    private void biocapture() {
        try {
            String pidOption = getPIDOptions();
            if (pidOption != null) {
                Intent intent2 = new Intent();
                intent2.setAction("in.gov.uidai.rdservice.fp.CAPTURE");
                intent2.setPackage("com.mantra.rdservice");
                intent2.putExtra("PID_OPTIONS", pidOption);
                startActivityForResult(intent2, 2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * Biomectirc device's capture data
     */
    private void biomorophoCapture() {
        try {
            String pidOption = getPIDOptions();
            if (pidOption != null) {
                Intent intent = new Intent("in.gov.uidai.rdservice.fp.CAPTURE");
                intent.setPackage("com.scl.rdservice");
                intent.putExtra("PID_OPTIONS", pidOption);
                startActivityForResult(intent, 4);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
     * data needed for the biometric device's
     *
     * for device info and the capture of the finger prints
     */
    private String getPIDOptions() {
        try {
            String posh = getResources().getString(R.string.posh);
            if (positions.size() > 0) {
                posh = positions.toString().replace("[", "").replace("]", "").replaceAll("[\\s+]", "");
            }

            Opts opts = new Opts();
            opts.fCount = "1";
            opts.fType = "0";
            opts.iCount = "0";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format = "0";
            opts.pidVer = "2.0";
            opts.timeout = "10000";
            opts.posh = posh;
            opts.env = "P";

            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = "1.0";
            pidOptions.Opts = opts;

            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void showLoader() {
        if (loadingView == null) {
            loadingView = new ProgressDialog(BioAuthActivity.this);
            loadingView.setCancelable(false);
            loadingView.setMessage("Please Wait..");
        }
        loadingView.show();
    }

    public void hideLoader() {
        try {
            if (loadingView!=null){
                loadingView.dismiss();
            }
        }catch (Exception e){
            //e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra("DEVICE_INFO");
                            String rdService = data.getStringExtra("RD_SERVICE_INFO");
                            String display = "";
                            if (rdService != null) {
                                display = "RD Service Info :\n" + rdService + "\n\n";
                            }
                            if (result != null) {
                            }
                        }
                    } catch (Exception e) {
                        hideLoader();
                    }
                }
                break;
            case 2:
                hideLoader();
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra("PID_DATA");
                            //mantra_result = result;
                            if (result != null) {
                                pidData = serializer.read(PidData.class, result);

                                if (Float.parseFloat(pidData._Resp.qScore) <= 60) {


                                    if (two_fact_depositBar != null) {
                                        two_fact_depositBar.setProgress((int) Float.parseFloat(pidData._Resp.qScore));
                                   /* two_fact_depositBar.setProgressTextMoved(true);
                                    two_fact_depositBar.setEndColor(getResources().getColor(R.color.red));
                                    two_fact_depositBar.setStartColor(getResources().getColor(R.color.red));
                                  */
                                    }


                                } else if (Float.parseFloat(pidData._Resp.qScore) >= 60 && Float.parseFloat(pidData._Resp.qScore) <= 70) {

                                    if (two_fact_depositBar != null) {
                                        two_fact_depositBar.setProgress((int) Float.parseFloat(pidData._Resp.qScore));
                                        two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                        two_fact_submitButton.setEnabled(true);
                                        two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                    }


                                } else {
                                    if (two_fact_depositBar != null) {
                                        two_fact_depositBar.setProgress((int) Float.parseFloat(pidData._Resp.qScore));
                                        two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                        two_fact_submitButton.setEnabled(true);
                                        two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                    }

                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;

            case 3:
                if (resultCode == RESULT_OK) {

                    try {
                        if (data != null) {
                            String result = data.getStringExtra("DEVICE_INFO");
                            String rdService = data.getStringExtra("RD_SERVICE_INFO");
                            String display = "";
                            if (rdService != null) {
                                display = "RD Service Info :\n" + rdService + "\n\n";
                            }
                            if (result != null) {
                            }
                        }
                    } catch (Exception e) {
                        hideLoader();
                    }

                }
                break;
            case 4:
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra("PID_DATA");


                            if (result != null) {
                                morphoPidData = serializer.read(MorphoPidData.class, result);

                                pidData = serializer.read(PidData.class, result);

                                if (Float.parseFloat(pidData._Resp.qScore) <= 50) {
                                    if (two_fact_depositBar != null) {
                                        two_fact_depositBar.setProgress((int) Float.parseFloat(pidData._Resp.qScore));
                                        Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show();
                                    }
                                } else if (Float.parseFloat(pidData._Resp.qScore) >= 50 && Float.parseFloat(pidData._Resp.qScore) <= 100) {

                                    if (two_fact_depositBar != null) {
                                        two_fact_depositBar.setProgress((int) Float.parseFloat(pidData._Resp.qScore));
                                        two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                        two_fact_submitButton.setEnabled(true);
                                        two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                    }


                                } else {

                                    if (two_fact_depositBar != null) {
                                        two_fact_depositBar.setProgress((int) Float.parseFloat(pidData._Resp.qScore));
                                        two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                        two_fact_submitButton.setEnabled(true);
                                        two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                    }

                                }

                            } else {
                            }
                            if (morphoPidData._Resp.errCode.equalsIgnoreCase("720")) {
                            } else if (morphoPidData._Resp.errCode.equalsIgnoreCase("0")) {
                            } else if (morphoPidData._Resp.errCode.equalsIgnoreCase("700")) {
                            } else if (morphoPidData._Resp.errCode.equalsIgnoreCase("730")) {
                            } else {
                            }
                        }
                    } catch (Exception e) {

                    }

                }
                break;
        }
    }

   /* @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 12:
                if(loadingView!=null){
                    loadingView.dismiss();
                }
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra ( "PID_DATA" );
                            if (result != null) {
                                pidData = serializer.read ( PidData.class, result );

                                if(Float.parseFloat ( pidData._Resp.qScore ) <=60){
                                    if(two_fact_depositBar!=null) {
                                        two_fact_depositBar.setProgress(0);
                                    }
                                }else if(Float.parseFloat ( pidData._Resp.qScore ) >=60 && Float.parseFloat ( pidData._Resp.qScore ) <=70){
                                    if(two_fact_depositBar!=null) {
                                        two_fact_depositBar.setProgress(70);
                                        two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                        two_fact_submitButton.setEnabled(true);
                                        two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                    }
                                }else{
                                    if(two_fact_depositBar!=null) {
                                        two_fact_depositBar.setProgress((int)Float.parseFloat(pidData._Resp.qScore));
                                        two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                        two_fact_submitButton.setEnabled(true);
                                        two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                    }
                                }
                            }else{
                                // Util.showAlert(AEPS2HomeActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.device_is_not_ready_error));
                            }
                            if (pidData._Resp.errCode.equalsIgnoreCase("720")){
                                // Util.showAlert ( AEPS2HomeActivity.this,getResources ().getString ( R.string.success ),getResources ().getString ( R.string.capture_success ) );
                            }else if(pidData._Resp.errCode.equalsIgnoreCase ( "700" )){
                                // Util.showAlert(AEPS2HomeActivity.this,getResources().getString(R.string.fail_error),pidData._Resp.errInfo + " " + ". Please Try Again !!!");
                            }else if(pidData._Resp.errCode.equalsIgnoreCase ( "730" )) {
                                //  Util.showAlert(AEPS2HomeActivity.this,getResources().getString(R.string.fail_error),"Capture stopped or Aborted. Please Try Again !!!");
                            }else if(Float.parseFloat ( pidData._Resp.qScore ) <=60){
                                // Util.showAlert(AEPS2HomeActivity.this,getResources().getString(R.string.fail_error),pidData._Resp.errInfo+ " " +"with a score less then 60% " + ". Please Try Again !!!");
                            }else{
                                // Util.showAlert(AEPS2HomeActivity.this,getResources().getString(R.string.fail_error),pidData._Resp.errInfo+ " " + ". Please Try Again !!!");
                            }

                        }
                    } catch (Exception e) {
                        if (loadingView != null) {
                            loadingView.dismiss();
                        }
                    }
                }
                break;
            case 14:
                if(loadingView!=null){
                    loadingView.dismiss();
                }
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra ( "PID_DATA" );
                            if (result != null) {
                                morphoPidData = serializer.read ( MorphoPidData.class, result );

                            }else{
                                Util.showAlert(BioAuthActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.device_is_not_ready_error));
                            }
                            if (morphoPidData._Resp.errCode.equalsIgnoreCase("720")){
                                Util.showAlert(BioAuthActivity.this,getResources().getString(R.string.fail_error),morphoPidData._Resp.errInfo );
                            }else if(morphoPidData._Resp.errCode.equalsIgnoreCase ( "0" )){
                                if(two_fact_depositBar!=null) {
                                    two_fact_depositBar.setProgress((int)Float.parseFloat(pidData._Resp.qScore));
                                    two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                    two_fact_submitButton.setEnabled(true);
                                    two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                }
                            }else if(morphoPidData._Resp.errCode.equalsIgnoreCase ( "700" )){
                                Util.showAlert(BioAuthActivity.this,getResources().getString(R.string.fail_error),morphoPidData._Resp.errInfo + " " + ". Please Try Again !!!");
                            }else if(morphoPidData._Resp.errCode.equalsIgnoreCase ( "730" )) {
                                Util.showAlert(BioAuthActivity.this,getResources().getString(R.string.fail_error),"Capture stopped or Aborted. Please Try Again !!!");
                            }else{
                                Util.showAlert(BioAuthActivity.this,getResources().getString(R.string.fail_error),morphoPidData._Resp.errInfo+ " " + ". Please Try Again !!!");

                            }
                        }
                    } catch (Exception e) {
                        if (loadingView != null) {
                            loadingView.dismiss();
                        }
                    }
                }
                break;

        }
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

}
