package com.matm.matmsdk.aepsmodule.transactionstatus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import isumatm.androidsdk.equitas.R;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.matm.matmsdk.aepsmodule.AEPS2HomeActivity;
import com.matm.matmsdk.aepsmodule.utils.AepsSdkConstants;
import com.matm.matmsdk.aepsmodule.utils.Session;
import com.matm.matmsdk.aepsmodule.utils.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class TransactionStatusActivity extends AppCompatActivity {

    LinearLayout successLayout,failureLayout;
    Button okButton,okSuccessButton;
    TextView detailsTextView,failureDetailTextView,failureTitleTextView;
    //String aadharCardStr ="";
    //String amount ="";
   // Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_statusssss);

       // session = new Session(TransactionStatusActivity.this);

        successLayout = findViewById(R.id.successLayout);
        failureLayout = findViewById(R.id.failureLayout);
        okButton = findViewById(R.id.okButton);
        okSuccessButton = findViewById(R.id.okSuccessButton);
        detailsTextView = findViewById(R.id.detailsTextView);
        failureTitleTextView = findViewById(R.id.failureTitleTextView);
        failureDetailTextView = findViewById(R.id.failureDetailTextView);

        if(getIntent().getSerializableExtra(AepsSdkConstants.TRANSACTION_STATUS_KEY) == null){
            failureLayout.setVisibility(View.VISIBLE);
            successLayout.setVisibility(View.GONE);
            failureDetailTextView .setText ( "Some Exception occured");
        }else{
            TransactionStatusModel transactionStatusModel = (TransactionStatusModel) getIntent().getSerializableExtra(AepsSdkConstants.TRANSACTION_STATUS_KEY);

            if (transactionStatusModel.getStatus ().trim ().equalsIgnoreCase ( "0" )) {
                failureLayout.setVisibility ( View.GONE );
                successLayout.setVisibility ( View.VISIBLE );
                String aadharCard = transactionStatusModel.getAadharCard ();
                //aadharCardStr = aadharCard;
                //amount = transactionStatusModel.getTransactionAmount();
                if (transactionStatusModel.getAadharCard () == null) {
                    aadharCard = "N/A";
                } else {
                    if(transactionStatusModel.getAadharCard ().equalsIgnoreCase ( "" )){
                        aadharCard = "N/A";
                    }else {
                        StringBuffer buf = new StringBuffer( aadharCard );
                        buf.replace ( 0, 10, "XXXX-XXXX-" );
                        System.out.println ( buf.length () );
                        aadharCard = buf.toString ();
                    }
                }

                String bankName = "N/A";
                if (transactionStatusModel.getBankName () != null && !transactionStatusModel.getBankName ().matches ( "" )) {
                    bankName = transactionStatusModel.getBankName ();
                }
                String referenceNo = "N/A";
                if (transactionStatusModel.getReferenceNo () != null && !transactionStatusModel.getReferenceNo ().matches ( "" )) {
                    referenceNo = transactionStatusModel.getReferenceNo ();
                }
                String balance = "N/A";
                if (transactionStatusModel.getBalanceAmount () != null && !transactionStatusModel.getBalanceAmount ().matches ( "" )) {
                    balance = transactionStatusModel.getBalanceAmount ();
                    if (balance.contains ( ":" )) {
                        String[] separated = balance.split ( ":" );
                        balance = separated[ 1 ].trim ();
                    }
                }
                String amount = "N/A";
                if (transactionStatusModel.getTransactionAmount () != null && !transactionStatusModel.getTransactionAmount ().matches ( "" )) {
                    amount = transactionStatusModel.getTransactionAmount ();
                }

                if(transactionStatusModel.getTransactionType ().equalsIgnoreCase ( "Cash Withdrawal" )) {
                    detailsTextView.setText ( transactionStatusModel.getTransactionType () + " for customer account linked with aadhar card " + aadharCard + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + referenceNo + " \n " + "Account Balance : " + balance + "\n Transaction Amount : " + amount );
                }else if(transactionStatusModel.getTransactionType ().equalsIgnoreCase ( "Balance Enquery" )){
                    detailsTextView.setText ( transactionStatusModel.getTransactionType () + " for customer account linked with aadhar card " + aadharCard + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + referenceNo + " \n " + "Account Balance : " + balance );
                }
            }else{
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                failureDetailTextView .setText (transactionStatusModel.getApiComment ());
                failureTitleTextView .setText (transactionStatusModel.getStatusDesc () );
            }
        }
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent respIntent = new Intent();
                setResult(Activity.RESULT_OK,respIntent);
                finish();
            }
        });
        okSuccessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent respIntent = new Intent();
                setResult(Activity.RESULT_OK,respIntent);
                finish();
            }
        });

    }


}
